namespace WorkflowBot.Services

open Funogram.Api
open Funogram.Telegram
open Funogram.Telegram.Bot
open Funogram.Telegram.Types
open Microsoft.Extensions.Logging
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Options
open System
open System.Threading.Channels
open WorkflowBot.Models.Events
open WorkflowBot.Services
open Storage
open Helpers
open System.Text

module TelegramBot =
    [<CLIMutable>]
    type TelegramBotSettings = {
        Token: string
        BitbucketWebhookHost: string
        GitlabWebhookHost: string
    }

    type TelegramBot(options: IOptions<TelegramBotSettings>, logger: ILogger<TelegramBot>, eventsQueue: ChannelReader<Guid * Event>,
    storage: Storage.WebhookStorage) =
        inherit BackgroundService()

        let botConfig = { Config.defaultConfig with Token = options.Value.Token }

        let withMarkdown messageRequest: Funogram.Telegram.Req.SendMessage = { messageRequest with ParseMode = (Some ParseMode.Markdown) }
        let withoutPreview messageRequest: Funogram.Telegram.Req.SendMessage = { messageRequest with DisableWebPagePreview = (Some true) }
        let sendMessage chatId text =
            Api.sendMessage chatId text
            |> withMarkdown
            |> withoutPreview
            |> api botConfig
            |> Async.Ignore
            |> Async.Start
        let deleteMessage (message: Message) =
            Api.deleteMessage message.Chat.Id message.MessageId
            |> api botConfig
            |> Async.Ignore
            |> Async.Start
        let sendKeyboard chatId text keyboardId buttons =
            let keyboardButton (text, value) = InlineKeyboardButton.Create(text = text, callbackData = $"{keyboardId}|{value}")
            let markup = InlineKeyboardMarkup { InlineKeyboard = buttons |> Array.map ^ Array.map keyboardButton }

            Api.sendMessageMarkup chatId text markup
            |> api botConfig
            |> Async.Ignore
            |> Async.Start

        let channelCmd commandTemplate handler context =
            match context.Update.ChannelPost with
            | Some post ->
                let mentionEntity = post.Entities |> Option.bind ^ Seq.tryFind (fun item -> item.Type = "mention")
                let commandEntity = post.Entities |> Option.bind ^ Seq.tryFind (fun item -> item.Type = "bot_command")
                match (mentionEntity, commandEntity) with
                | (Some mention, Some command) ->
                    let mentionedUsername = (Option.get post.Text).Substring(int(mention.Offset), int(mention.Length)).TrimStart('@')
                    let commandText = (Option.get post.Text).Substring(int(command.Offset), int(command.Length))
                    let myUserName = context.Me.Username.Value
                    match () with
                    | _ when myUserName = mentionedUsername && commandText = commandTemplate ->
                        handler context
                        true
                    | _ -> false
                | _ -> false
            | None -> false

        let keyboardCallback keyboardId handler context =
            match context.Update.CallbackQuery with
            | Some callback ->
                match callback.Data with
                | Some data ->
                    let parts = data.Split('|') |> Array.toList
                    match parts with
                    | first::second::_ when first = keyboardId ->
                        handler second
                        deleteMessage callback.Message.Value
                        true
                    | _ -> false
                | _ -> false
            | None -> false

        let webhookUrlMessage webhookId webhookType =
            let baseUrl = match webhookType with
                            | Gitlab -> options.Value.GitlabWebhookHost
                            | Bitbucket -> options.Value.BitbucketWebhookHost
            $"Use {baseUrl}{webhookId} as your webhook url."

        override this.ExecuteAsync(token) =
            task {
                    startBot botConfig this.UpdateArrived None |> Async.Start
                    let mutable completed = false
                    while not completed do
                        let! haveNextValue = eventsQueue.WaitToReadAsync token
                        completed <- not haveNextValue
                        if not completed then
                            let! (webhookId, event) = eventsQueue.ReadAsync token
                            logger.LogInformation("Received event on webhook {}", webhookId)
                            let chatId = storage.GetWebhookChatId webhookId
                            match chatId with
                            | Some id ->
                                let message = MessageGenerator.getMessage event
                                match message with
                                | _ when not ^ String.IsNullOrEmpty message  -> sendMessage id message
                                | _ -> ()
                            | _ -> ()

                    ()
                }

        member private this.UpdateArrived(ctx: UpdateContext) =
            let getChatId context =
                match context.Update.Message, context.Update.ChannelPost, context.Update.CallbackQuery with
                | Some message, _, _ ->
                    Some message.Chat.Id
                | _, Some post, _ ->
                    Some post.Chat.Id
                | _, _, Some callback ->
                    Some callback.Message.Value.Chat.Id
                | _ -> None
            let decideType context =
                let chatId = getChatId context
                match chatId with
                | Some chatId ->
                    sendKeyboard chatId "Choose webhook type" "newHook" [| [| ("Bitbucket", "bitbucket"); ("Gitlab", "gitlab") |] |]
                |None -> ()
            let getOrInit context =
                let chatId = getChatId context
                match chatId with
                | Some chatId ->
                    let webhooks = storage.GetWebhooks chatId
                    match webhooks with
                    | [] -> decideType context
                    | _ ->
                        let builder =
                            StringBuilder()
                            |> appendLine "Found webhooks:"
                            |> appendLine ^ (
                                webhooks
                                |> List.map ^ fun webhook -> $"Found `{webhook.Id}` webhook for {webhook.Type}. {webhookUrlMessage webhook.Id webhook.Type}"
                                |> partial String.Join Environment.NewLine )
                            |> appendLine "To create new webhook use /initNew"
                        sendMessage chatId ^ builder.ToString() |> ignore
                | None -> ()
            let init webhookId webhookTypeString context =
                let chatId = getChatId context
                let webhookType = parseWebhookType webhookTypeString
                match chatId with
                | Some chatId ->
                    storage.AddWebhook { Id = webhookId; ChatId = chatId; Type = webhookType }
                    sendMessage chatId $"{webhookType} webhook created `{webhookId}`.{Environment.NewLine} {webhookUrlMessage webhookId webhookType}" |> ignore
                | None -> ()
            let welcomeMessage context =
                let chatId = getChatId context
                match chatId with
                | Some chatId ->
                    sendMessage chatId $"To check your webhooks or add new use /init" |> ignore
                | None -> ()
            processCommands ctx [|
                cmd "/start" welcomeMessage
                cmd "/init" getOrInit
                cmd "/initNew" decideType
                cmdScan "/initExisting %s %s" (fun (webhookIdString, webhookType) ctx ->
                    let webhookId = Guid.Parse webhookIdString
                    init webhookId webhookType ctx)
                keyboardCallback "newHook" (fun webhookType -> init (Guid.NewGuid()) webhookType ctx)
                channelCmd "/start" welcomeMessage
                channelCmd "/init" getOrInit
                channelCmd "/initNew" decideType
            |] |> ignore
