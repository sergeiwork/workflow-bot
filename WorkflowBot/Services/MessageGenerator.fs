namespace WorkflowBot.Services

open WorkflowBot.Models.Events
open Helpers
open System.Text
open System

module MessageGenerator =
    let constructPushMessage event =
        let push =
            match event with
            | Push push when not push.IsBranchClosed -> Some push
            | Push _ -> None
            | _ -> raise (Exception())
        match push with
        | Some push ->
            let builder =
                StringBuilder()
                |> appendLine ^ sprintf "*%s* [%spushed](%s) to [%s](%s)/[%s](%s)" push.ActorName (if push.IsForced then "😱 forcibly " else "") push.ChangesLink push.RepositoryFullName push.RepositoryLink push.BranchFullName push.BranchLink
                |> appendLine ^ (
                    push.Commits
                    |> List.map ^ fun commit -> sprintf "[%s](%s)" commit.Message commit.Link
                    |> partial String.Join Environment.NewLine )
                |> if push.IsTruncated then appendLine ^ sprintf "[... See all](%s)" push.BranchLink else skip
            builder.ToString()
        | None -> String.Empty

    let constructPullRequestCreatedMessage event =
        let pullRequest =
            match event with
            | PullRequestCreated pullRequest -> pullRequest
            | _ -> raise (Exception())
        let builder =
            StringBuilder()
            |> appendLine ^ sprintf "Pull request [%s](%s) created" pullRequest.Title pullRequest.Link
            |> appendLine ^ sprintf "in repository [%s](%s)" pullRequest.RepositoryFullName pullRequest.RepositoryLink
            |> appendLine "Author:"
            |> appendLine ^ sprintf "🤛 %s" pullRequest.Author
            |> appendLine ^ if pullRequest.Reviewers.Length > 0 then "Reviewers:" else String.Empty
            |> appendLine ^ (
                pullRequest.Reviewers
                |> List.map ^ sprintf "✊ %s"
                |> partial String.Join Environment.NewLine )
        builder.ToString()

    let constructPullRequestApprovedMessage event =
        let pullRequest =
            match event with
            | PullRequestApproved pullRequest -> pullRequest
            | _ -> raise (Exception())
        let builder =
            StringBuilder()
            |> appendLine ^ sprintf "Pull request [%s](%s) in [%s](%s) approved by" pullRequest.Title pullRequest.Link pullRequest.RepositoryFullName pullRequest.RepositoryLink
            |> appendLine ^ sprintf "👌 %s" pullRequest.ApprovedBy
        builder.ToString()

    let constructPullRequestMergedMessage event =
        let pullRequest =
            match event with
            | PullRequestMerged pullRequest -> pullRequest
            | _ -> raise (Exception())
        let builder =
            StringBuilder()
            |> appendLine ^ sprintf "Pull request [%s](%s) in [%s](%s) merged by" pullRequest.Title pullRequest.Link pullRequest.RepositoryFullName pullRequest.RepositoryLink
            |> appendLine ^ sprintf "🤝 %s" pullRequest.ClosedBy
        builder.ToString()

    let getMessage payload =
        let message =
            match payload with
            | Push _ -> constructPushMessage payload
            | PullRequestCreated _ -> constructPullRequestCreatedMessage payload
            | PullRequestApproved _ -> constructPullRequestApprovedMessage payload
            | PullRequestMerged _ -> constructPullRequestMergedMessage payload
            | _ -> String.Empty
        message
