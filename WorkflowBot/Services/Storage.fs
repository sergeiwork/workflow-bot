namespace WorkflowBot.Services

open Microsoft.Extensions.Options
open Npgsql.FSharp
open System
open Helpers

module Storage =
    [<CLIMutable>]
    type WebhookStorageSettings = {
        ConnectionString: string
    }

    type WebhookType = Bitbucket | Gitlab
    let parseWebhookType (webhookType:string) =
        match webhookType.ToLower() with
        | "bitbucket" -> Bitbucket
        | "gitlab" -> Gitlab
        | _ -> raise (ArgumentOutOfRangeException(nameof(webhookType)))

    type Webhook = {
        Id: Guid
        ChatId: int64
        Type: WebhookType
    }

    type WebhookStorage(options: IOptions<WebhookStorageSettings>) =
        let settings = options.Value
        member this.AddWebhook(webhook) =
            settings.ConnectionString
            |> Sql.connect
            |> Sql.query "INSERT INTO webhooks.webhook VALUES (@id, @chat_id, @type)"
            |> Sql.parameters [
                "@id", Sql.uuid webhook.Id;
                "@chat_id", Sql.int64 webhook.ChatId;
                "@type", Sql.string (webhook.Type.ToString()) ]
            |> Sql.executeNonQuery |> ignore
            ()
        member this.GetWebhookChatId webhookId =
            settings.ConnectionString
            |> Sql.connect
            |> Sql.query "SELECT chat_id FROM webhooks.webhook WHERE id = @id"
            |> Sql.parameters [ "@id", Sql.uuid webhookId ]
            |> Sql.execute(fun read -> read.int64 "chat_id")
            |> List.tryExactlyOne
        member this.GetWebhookType webhookId =
            settings.ConnectionString
            |> Sql.connect
            |> Sql.query "SELECT type FROM webhooks.webhook WHERE id = @id"
            |> Sql.parameters [ "@id", Sql.uuid webhookId ]
            |> Sql.execute(fun read -> read.string "type")
            |> List.exactlyOne
            |> fun s -> parseWebhookType s
        member this.GetWebhooks chatId =
            settings.ConnectionString
            |> Sql.connect
            |> Sql.query "SELECT id, type, chat_id FROM webhooks.webhook WHERE chat_id = @chatId"
            |> Sql.parameters [ "@chatId", Sql.int64 chatId ]
            |> Sql.execute (fun read ->
                let id = read.uuid "id"
                let webhookType = parseWebhookType ^ read.string "type"
                let chatId = read.int64 "chat_id"
                { Id = id; Type = webhookType; ChatId = chatId })
