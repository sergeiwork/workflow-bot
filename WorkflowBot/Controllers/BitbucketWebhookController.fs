namespace WorkflowBot.Controllers

open System
open System.Linq
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System.IO
open System.Text.Json
open WorkflowBot.Models.Bitbucket
open System.Text.Json.Nodes
open System.Threading.Channels
open WorkflowBot.Services
open WorkflowBot.Models.Events
open WorkflowBot.Services.Storage
open WorkflowBot.Converters

[<ApiController>]
[<Route("[controller]")>]
type BitbucketWebhookController (logger : ILogger<BitbucketWebhookController>, channelWriter: ChannelWriter<Guid * Event>,
    storage: Storage.WebhookStorage, converter: BitbucketConverter) =
    inherit ControllerBase()
    let eventsQueue = channelWriter

    [<HttpPost("{id}")>]
    member this.Webhook(id: Guid) =
        task {
            let webhookType = storage.GetWebhookType id

            match webhookType with
            | Bitbucket ->
                let eventType = this.HttpContext.Request.Headers["X-Event-Key"].FirstOrDefault()

                let options = JsonSerializerOptions()
                options.PropertyNameCaseInsensitive <- true
                let streamReader = new StreamReader(this.HttpContext.Request.Body)
                let! body = streamReader.ReadToEndAsync()

                let bodyObject = JsonObject.Parse(body)
                let actor = bodyObject["actor"].Deserialize<Common.User>(options)
                let repository = bodyObject["repository"].Deserialize<Common.Repository>(options)

                let event =
                    match eventType with
                    | "repo:push" -> Payload.Event.Push (bodyObject.Deserialize<Push.Event>(options))
                    | "pullrequest:created" -> Payload.Event.PullRequestCreated (bodyObject.Deserialize<PullRequest.Created>(options))
                    | "pullrequest:approved" -> Payload.Event.PullRequestApproved (bodyObject.Deserialize<PullRequest.Approved>(options))
                    | "pullrequest:fulfilled" -> Payload.Event.PullRequestMerged (bodyObject.Deserialize<PullRequest.Merged>(options))
                    | _ -> raise (Exception())

                let payload: Payload.Payload = { Actor = actor; Repository = repository; Event = event }
                let event = converter.GetEvent payload
                do! eventsQueue.WriteAsync((id, event))

                return this.Ok() :> IActionResult
            | _ -> return this.NotFound()
        }
