namespace WorkflowBot.Controllers

open System
open System.Linq
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System.IO
open System.Text.Json
open WorkflowBot.Models.Gitlab
open System.Threading.Channels
open WorkflowBot.Services
open WorkflowBot.Services.Storage
open WorkflowBot.Models.Events
open WorkflowBot.Converters

[<ApiController>]
[<Route("[controller]")>]
type GitlabWebhookController (logger : ILogger<GitlabWebhookController>, channelWriter: ChannelWriter<Guid * Event>,
    storage: Storage.WebhookStorage, converter: GitlabConverter) =
    inherit ControllerBase()
    let eventsQueue = channelWriter

    [<HttpPost("{id}")>]
    member this.Webhook(id: Guid) =
        task {
            let webhookType = storage.GetWebhookType id

            match webhookType with
            | Gitlab ->
                let eventType = this.HttpContext.Request.Headers["X-Gitlab-Event"].FirstOrDefault()

                let options = JsonSerializerOptions()
                options.PropertyNameCaseInsensitive <- true
                let streamReader = new StreamReader(this.HttpContext.Request.Body)
                let! body = streamReader.ReadToEndAsync()

                let payload =
                    match eventType with
                    | "Push event" -> Payload.Push (JsonSerializer.Deserialize<Push.Root>(body, options))
                    | "Merge request event" -> Payload.PullRequest (JsonSerializer.Deserialize<PullRequest.Root>(body, options))
                    | _ -> raise (Exception())

                let event = converter.GetEvent payload

                do! eventsQueue.WriteAsync((id, event))

                return this.Ok() :> IActionResult
            | _ -> return this.NotFound()
        }
