module Helpers

open System.Text

let inline (^) f x = f x
let inline partial f x y = f(x, y)
let appendLine (line:string) (builder:StringBuilder) = builder.AppendLine line
let skip x = x
