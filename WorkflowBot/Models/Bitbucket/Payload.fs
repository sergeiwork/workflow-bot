namespace WorkflowBot.Models.Bitbucket

open Common

module Payload =
    type Event =
        | Push of Push.Event
        | PullRequestCreated of PullRequest.Created
        | PullRequestApproved of PullRequest.Approved
        | PullRequestMerged of PullRequest.Merged

    type Payload =
        { Actor: User
          Repository: Repository
          Event: Event }
