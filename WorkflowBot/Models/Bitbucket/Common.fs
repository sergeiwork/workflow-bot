namespace WorkflowBot.Models.Bitbucket

open System.Text.Json.Serialization

module Common =
    type Link = { Href: string }

    type EntityLinks =
        { Self: Link
          Html: Link
          Avatar: Link }

    type Project =
        { Type: string
          Name: string
          Uuid: string
          Links: EntityLinks
          Key: string }

    type User =
        { Type: string
          Username: string
          Nickname: string
          [<JsonPropertyName("display_name")>]
          DisplayName: string
          Uuid: string
          Links: EntityLinks }

    type Workspace =
        { Type: string
          Name: string
          Slug: string
          Uuid: string
          Links: EntityLinks }

    type Repository =
        { Type: string
          Links: EntityLinks
          Uuid: string
          Project: Project
          [<JsonPropertyName("full_name")>]
          FullName: string
          Workspace: Workspace
          Name: string
          Website: string
          Scm: string
          [<JsonPropertyName("is_private")>]
          IsPrivate: bool
          Owner: User }
