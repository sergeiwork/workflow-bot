namespace WorkflowBot.Models.Bitbucket

open Common
open System
open System.Text.Json.Serialization

module PullRequest =
    type Summary =
        { Type: string
          Raw: string
          Markup: string
          Html: string }

    type PullRequestLinks =
        { Self: Link
          Html: Link
          Commits: Link
          Approve: Link
          RequestChanges: Link
          Diff: Link
          Diffstat: Link
          Comments: Link
          Activity: Link
          Merge: Link
          Decline: Link
          Statuses: Link }

    type CommitLinks =
        { Self: Link
          Html: Link }

    type Commit =
        { Type: string
          Hash: string
          Links: CommitLinks }

    type Branch = { Name: string }

    type Source =
        { Branch: Branch
          Commit: Commit
          Repository: Repository }

    type Rendered =
        { Title: Summary
          Description: Summary }

    type Participant =
        { Type: string
          User: User
          Role: string
          Approved: bool
          State: string
          [<JsonPropertyName("participated_on")>]
          ParticipatedOn: string }

    type PullRequest =
        {
          [<JsonPropertyName("comment_count")>]
          CommentCount: int64
          [<JsonPropertyName("task_count")>]
          TaskCount: int64
          Type: string
          Id: int64
          Title: string
          Description: string
          Rendered: Rendered
          State: string
          [<JsonPropertyName("merge_commit")>]
          MergeCommit: Commit option
          [<JsonPropertyName("close_source_branch")>]
          CloseSourceBranch: bool
          [<JsonPropertyName("closed_by")>]
          ClosedBy: User option
          Author: User
          Reason: string
          [<JsonPropertyName("created_on")>]
          CreatedOn: string
          [<JsonPropertyName("updated_on")>]
          UpdatedOn: string
          Destination: Source
          Source: Source
          Reviewers: User list
          Participants: Participant list
          Links: PullRequestLinks
          Summary: Summary }

    type Approval =
        { Date: DateTime
          User: User }

    type Created =
        { PullRequest: PullRequest }

    type Approved =
        { PullRequest: PullRequest
          Approval: Approval }

    type Merged =
        { PullRequest: PullRequest }
