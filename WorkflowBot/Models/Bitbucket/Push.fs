namespace WorkflowBot.Models.Bitbucket

open Common

module Push =
    type CommonLinks =
        { Self: Link
          Html: Link }

    type CommitLinks =
        { Self: Link
          Html: Link
          Diff: Link
          Approve: Link
          Comments: Link
          Statuses: Link
          Patch: Link }

    type ChangesLinks =
        { Commits: Link
          Diff: Link
          Html: Link }

    type BranchLinks =
        { Self: Link
          Commits: Link
          Html: Link }

    type Parents =
        { Type: string
          Hash: string
          Links: CommonLinks }

    type Summary =
        { Type: string
          Raw: string
          Markup: string
          Html: string }

    type Author =
        { Type: string
          Raw: string
          User: User }

    type Commits =
        { Type: string
          Hash: string
          Date: string
          Author: Author
          Message: string
          Summary: Summary
          Links: CommitLinks
          Parents: Parents list
          Rendered: obj
          Properties: obj }

    type Target =
        { Type: string
          Hash: string
          Date: string
          Author: Author
          Message: string
          Summary: Summary
          Links: CommonLinks
          Parents: Parents list
          Rendered: obj
          Properties: obj }

    type Branch =
        { Name: string
          Target: Target
          Links: BranchLinks
          Type: string
          MergeStrategies: string list
          DefaultMergeStrategy: string }

    type Changes =
        { Old: Branch
          New: Branch
          Truncated: bool
          Created: bool
          Forced: bool
          Closed: bool
          Links: ChangesLinks
          Commits: Commits list }

    type Push = { Changes: Changes list }

    type Event = { Push: Push }
