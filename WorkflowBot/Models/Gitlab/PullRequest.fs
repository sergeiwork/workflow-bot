namespace WorkflowBot.Models.Gitlab

open Common
open System.Text.Json.Serialization

module PullRequest =
    type Reviewers =
        { Id: int64
          Name: string
          Username: string
          [<JsonPropertyName("avatar_url")>]
          AvatarUrl: string }
    type LastEditedById =
        { Previous: int64
          Current: int64 }
    type LastEditedAt =
        { Previous: string
          Current: string }
    type Current =
        { Id: int64
          Title: string
          Color: string
          [<JsonPropertyName("project_id")>]
          ProjectId: int64
          [<JsonPropertyName("created_at")>]
          CreatedAt: string
          [<JsonPropertyName("updated_at")>]
          UpdatedAt: string
          Template: bool
          Description: string
          Type: string
          [<JsonPropertyName("group_id")>]
          GroupId: int64 }
    type Labels =
        { Previous: Current list
          Current: Current list }
    type UpdatedAt =
        { Previous: string
          Current: string }
    type Draft =
        { Previous: bool
          Current: bool }
    type Changes =
        { [<JsonPropertyName("updated_by_id")>]
          UpdatedById: LastEditedById
          Draft: Draft
          [<JsonPropertyName("updated_at")>]
          UpdatedAt: UpdatedAt
          Labels: Labels
          [<JsonPropertyName("last_edited_at")>]
          LastEditedAt: LastEditedAt
          [<JsonPropertyName("last_edited_by_id")>]
          LastEditedById: LastEditedById }
    type Author =
        { Name: string
          Email: string }
    type LastCommit =
        { Id: string
          Message: string
          Title: string
          Timestamp: string
          Url: string
          Author: Author }
    type Target =
        { Name: string
          Description: string
          [<JsonPropertyName("web_url")>]
          WebUrl: string
          [<JsonPropertyName("avatar_url")>]
          AvatarUrl: string
          [<JsonPropertyName("git_ssh_url")>]
          GitSshUrl: string
          [<JsonPropertyName("git_http_url")>]
          GitHttpUrl: string
          Namespace: string
          [<JsonPropertyName("visibility_level")>]
          VisibilityLevel: int64
          [<JsonPropertyName("path_with_namespace")>]
          PathWithNamespace: string
          [<JsonPropertyName("default_branch")>]
          DefaultBranch: string
          Homepage: string
          Url: string
          [<JsonPropertyName("ssh_url")>]
          SshUrl: string
          [<JsonPropertyName("http_url")>]
          HttpUrl: string }
    type ObjectAttributes =
        { Id: int64
          Iid: int64
          [<JsonPropertyName("target_branch")>]
          TargetBranch: string
          [<JsonPropertyName("source_branch")>]
          SourceBranch: string
          [<JsonPropertyName("source_project_id")>]
          SourceProjectId: int64
          [<JsonPropertyName("author_id")>]
          AuthorId: int64
          [<JsonPropertyName("assignee_ids")>]
          AssigneeIds: int64 list
          [<JsonPropertyName("assignee_id")>]
          AssigneeId: int64
          [<JsonPropertyName("reviewer_ids")>]
          ReviewerIds: int64 list
          Title: string
          [<JsonPropertyName("created_at")>]
          CreatedAt: string
          [<JsonPropertyName("updated_at")>]
          UpdatedAt: string
          [<JsonPropertyName("last_edited_at")>]
          LastEditedAt: string
          [<JsonPropertyName("last_edited_by_id")>]
          LastEditedById: int64
          [<JsonPropertyName("milestone_id")>]
          MilestoneId: int64
          [<JsonPropertyName("state_id")>]
          StateId: int64
          State: string
          [<JsonPropertyName("blocking_discussions_resolved")>]
          BlockingDiscussionsResolved: bool
          [<JsonPropertyName("work_in_progress")>]
          WorkInProgress: bool
          Draft: bool
          [<JsonPropertyName("first_contribution")>]
          FirstContribution: bool
          [<JsonPropertyName("merge_status")>]
          MergeStatus: string
          [<JsonPropertyName("target_project_id")>]
          TargetProjectId: int64
          Description: string
          [<JsonPropertyName("total_time_spent")>]
          TotalTimeSpent: int64
          [<JsonPropertyName("time_change")>]
          TimeChange: int64
          [<JsonPropertyName("human_total_time_spent")>]
          HumanTotalTimeSpent: string
          [<JsonPropertyName("human_time_change")>]
          HumanTimeChange: string
          [<JsonPropertyName("human_time_estimate")>]
          HumanTimeEstimate: string
          Url: string
          Source: Target
          Target: Target
          [<JsonPropertyName("last_commit")>]
          LastCommit: LastCommit
          Labels: Current list
          Action: string
          [<JsonPropertyName("detailed_merge_status")>]
          DetailedMergeStatus: string }
    type User =
        { Id: int64
          Name: string
          Username: string
          [<JsonPropertyName("avatar_url")>]
          AvatarUrl: string
          Email: string }
    type Root =
        { [<JsonPropertyName("object_kind")>]
          ObjectKind: string
          [<JsonPropertyName("event_type")>]
          EventType: string
          User: User
          Project: Project
          Repository: Repository
          [<JsonPropertyName("object_attributes")>]
          ObjectAttributes: ObjectAttributes
          Labels: Current list
          Changes: Changes
          Assignees: Reviewers list
          Reviewers: Reviewers list }
