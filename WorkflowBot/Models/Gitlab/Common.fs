namespace WorkflowBot.Models.Gitlab

open System.Text.Json.Serialization

module Common =
    type Repository =
        { Name: string
          Url: string
          Description: string
          Homepage: string
          GitHttpUrl: string
          GitSshUrl: string
          VisibilityLevel: int64 }

    type Project =
        { Id: int64
          Name: string
          Description: string
          [<JsonPropertyName("web_url")>]
          WebUrl: string
          [<JsonPropertyName("avatar_url")>]
          AvatarUrl: string
          [<JsonPropertyName("git_ssh_url")>]
          GitSshUrl: string
          [<JsonPropertyName("git_http_url")>]
          GitHttpUrl: string
          Namespace: string
          [<JsonPropertyName("visibility_level")>]
          VisibilityLevel: int64
          [<JsonPropertyName("path_with_namespace")>]
          PathWithNamespace: string
          [<JsonPropertyName("default_branch")>]
          DefaultBranch: string
          [<JsonPropertyName("ci_config_path")>]
          CiConfigPath: string
          Homepage: string
          Url: string
          [<JsonPropertyName("ssh_url")>]
          SshUrl: string
          [<JsonPropertyName("http_url")>]
          HttpUrl: string }
