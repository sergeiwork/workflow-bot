namespace WorkflowBot.Models.Gitlab

open Common
open System.Text.Json.Serialization

module Push =
    type Author =
        { Name: string
          Email: string }
     type Commits =
        { Id: string
          Message: string
          Title: string
          Timestamp: string
          Url: string
          Author: Author
          Added: string list
          Modified: string list
          Removed: string list }
     type Root =
        { [<JsonPropertyName("object_kind")>]
          ObjectKind: string
          [<JsonPropertyName("event_name")>]
          EventName: string
          Before: string
          After: string
          Ref: string
          [<JsonPropertyName("ref_protected")>]
          RefProtected: bool
          [<JsonPropertyName("checkout_sha")>]
          CheckoutSha: string
          [<JsonPropertyName("user_id")>]
          UserId: int64
          [<JsonPropertyName("user_name")>]
          UserName: string
          [<JsonPropertyName("user_username")>]
          UserUsername: string
          [<JsonPropertyName("user_email")>]
          UserEmail: string
          [<JsonPropertyName("user_avatar")>]
          UserAvatar: string
          [<JsonPropertyName("project_id")>]
          ProjectId: int64
          Project: Project
          Repository: Repository
          Commits: Commits list
          [<JsonPropertyName("total_commits_count")>]
          TotalCommitsCount: int64 }
