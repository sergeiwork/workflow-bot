namespace WorkflowBot.Models.Gitlab

module Payload =
    type Payload =
        | Push of Push.Root
        | PullRequest of PullRequest.Root
