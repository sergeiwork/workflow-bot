namespace WorkflowBot.Models

module Events =
    type Commit =
        { Message: string
          Link: string }
    type Push =
        { IsBranchClosed: bool
          ActorName: string
          ChangesLink: string
          IsForced: bool
          RepositoryFullName: string
          RepositoryLink: string
          BranchFullName: string
          BranchLink: string
          Commits: Commit list
          IsTruncated: bool }
    type PullRequestCreated =
        { Title: string
          Link: string
          RepositoryFullName: string
          RepositoryLink: string
          Author: string
          Reviewers: string list }
    type PullRequestApproved =
        { Title: string
          Link: string
          RepositoryFullName: string
          RepositoryLink: string
          ApprovedBy: string }
    type PullRequestMerged =
        { Title: string
          Link: string
          RepositoryFullName: string
          RepositoryLink: string
          ClosedBy: string }
    type Event =
    | Push of Push
    | PullRequestCreated of PullRequestCreated
    | PullRequestApproved of PullRequestApproved
    | PullRequestMerged of PullRequestMerged
    | Empty

