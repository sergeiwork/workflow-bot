namespace WorkflowBot.Converters

open WorkflowBot.Models.Bitbucket
open WorkflowBot.Models.Events
open WorkflowBot.Models
open WorkflowBot.Models.Bitbucket.Payload

type BitbucketConverter =
    member public _.GetEvent payload =
        let repositoryFullName = payload.Repository.FullName
        let repositoryLink = payload.Repository.Links.Html.Href

        let convertPush (push: Push.Event) =
            let getCommit (commit: Push.Commits) = { Message = commit.Message; Link = commit.Links.Html.Href; }
            let push =
                { IsBranchClosed = push.Push.Changes.Head.Closed
                  ActorName = payload.Actor.DisplayName
                  ChangesLink = push.Push.Changes.Head.New.Links.Html.Href
                  IsForced = push.Push.Changes.Head.Forced
                  RepositoryFullName = repositoryFullName
                  RepositoryLink = repositoryLink
                  BranchFullName = push.Push.Changes.Head.New.Name
                  BranchLink = push.Push.Changes.Head.New.Links.Html.Href
                  Commits = push.Push.Changes.Head.Commits |> List.map getCommit
                  IsTruncated = push.Push.Changes.Head.Truncated }
            Events.Push push

        let convertPullRequestCreated (pullRequest: PullRequest.PullRequest) =
            let getReviewer (reviewer: Common.User) = reviewer.DisplayName
            let pullRequest =
                { Title = pullRequest.Title
                  Link = pullRequest.Links.Html.Href
                  RepositoryFullName = repositoryFullName
                  RepositoryLink = repositoryLink
                  Author = pullRequest.Author.DisplayName
                  Reviewers = pullRequest.Reviewers |> List.map getReviewer }
            Events.PullRequestCreated pullRequest

        let convertPullRequestApproved (pullRequest: PullRequest.Approved) =
            let pullRequest =
                { Title = pullRequest.PullRequest.Title
                  Link = pullRequest.PullRequest.Links.Html.Href
                  RepositoryFullName = repositoryFullName
                  RepositoryLink = repositoryLink
                  ApprovedBy = pullRequest.Approval.User.DisplayName }
            Events.PullRequestApproved pullRequest

        let convertPullRequestMerged (pullRequest: PullRequest.PullRequest) =
            let pullRequest =
                { Title = pullRequest.Title
                  Link = pullRequest.Links.Html.Href
                  RepositoryFullName = repositoryFullName
                  RepositoryLink = repositoryLink
                  ClosedBy = pullRequest.ClosedBy.Value.DisplayName }
            Events.PullRequestMerged pullRequest

        match payload.Event with
        | Push push -> convertPush push
        | PullRequestCreated pullRequest -> convertPullRequestCreated pullRequest.PullRequest
        | PullRequestApproved pullRequest -> convertPullRequestApproved pullRequest
        | PullRequestMerged pullRequest -> convertPullRequestMerged pullRequest.PullRequest