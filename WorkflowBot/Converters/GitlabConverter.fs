namespace WorkflowBot.Converters

open WorkflowBot.Models.Gitlab
open WorkflowBot.Models.Gitlab.Payload
open WorkflowBot.Models.Events
open WorkflowBot.Models

type GitlabConverter =
    member public _.GetEvent payload =
        let convertPush (push: Push.Root) =
            let getCommit (commit: Push.Commits) = { Message = commit.Message; Link = commit.Url; }
            let push =
                { IsBranchClosed = false
                  ActorName = push.UserName
                  ChangesLink = $"{push.Project.WebUrl}/-/compare/{push.Before}...{push.After}"
                  IsForced = false
                  RepositoryFullName = push.Repository.Name
                  RepositoryLink = push.Repository.Homepage
                  BranchFullName = push.Ref
                  BranchLink = $"{push.Project.WebUrl}/-/commits/{push.Ref}"
                  Commits = push.Commits |> List.truncate 5 |> List.map getCommit
                  IsTruncated = push.TotalCommitsCount > 5 }
            Events.Push push

        let convertPullRequest pullRequest = Events.Empty

        match payload with
        | Payload.Push push -> convertPush push
        | Payload.PullRequest pullRequest -> convertPullRequest pullRequest

