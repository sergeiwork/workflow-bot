namespace WorkflowBot

open System
open System.Threading.Channels
open WorkflowBot.Models.Events
open WorkflowBot.Services
open WorkflowBot.Services.TelegramBot

#nowarn "20"
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting

module Program =
    let exitCode = 0

    [<EntryPoint>]
    let main args =

        let builder = WebApplication.CreateBuilder(args)

        let channelOptions = UnboundedChannelOptions()
        channelOptions.SingleReader <- true
        builder.Services.AddSingleton(Channel.CreateUnbounded<Guid * Event>(channelOptions));
        builder.Services.AddSingleton<ChannelReader<Guid * Event>>(
            fun services -> services.GetRequiredService<Channel<Guid * Event>>().Reader);
        builder.Services.AddSingleton<ChannelWriter<Guid * Event>>(
            fun services -> services.GetRequiredService<Channel<Guid * Event>>().Writer);

        builder.Services.AddSingleton<Storage.WebhookStorage>().AddOptions<Storage.WebhookStorageSettings>().BindConfiguration("WebhookStorage")

        builder.Services.AddSingleton<Converters.BitbucketConverter>()
        builder.Services.AddSingleton<Converters.GitlabConverter>()

        builder.Services.AddHostedService<TelegramBot>().AddOptions<TelegramBotSettings>().BindConfiguration("TelegramBot")
        builder.Services.AddLogging()
        builder.Services.AddControllers()

        let app = builder.Build()

        app.UseHttpsRedirection()

        app.UseAuthorization()
        app.MapControllers()

        app.Run()

        exitCode
